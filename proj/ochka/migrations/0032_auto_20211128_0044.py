# Generated by Django 3.1 on 2021-11-27 19:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0031_auto_20211128_0041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metrikarequest',
            name='sort_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sort_param', to='ochka.metrikaparam', verbose_name='По какому параметру сортируем?'),
        ),
    ]
