# Generated by Django 3.1 on 2021-04-06 15:29

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0021_setting_metrika_request'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='html_end',
            field=tinymce.models.HTMLField(blank=True, null=True, verbose_name='Вторая часть HTML для вставки в отчет'),
        ),
    ]
