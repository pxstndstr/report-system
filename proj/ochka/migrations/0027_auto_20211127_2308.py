# Generated by Django 3.1 on 2021-11-27 18:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0026_remove_metrikaparam_sortable'),
    ]

    operations = [
        migrations.CreateModel(
            name='MetrikaDimension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=400, verbose_name='Имя параметра группировки')),
                ('param', models.CharField(max_length=400, verbose_name='Код параметра группировки')),
                ('priority', models.IntegerField(default=500, verbose_name='Приоритет в списке настроек')),
            ],
            options={
                'verbose_name': 'Параметр запроса к API Метрики',
                'verbose_name_plural': 'Параметры запроса к API Метрики',
                'ordering': ('priority',),
            },
        ),
        migrations.AlterModelOptions(
            name='metrikaparam',
            options={},
        ),
        migrations.RemoveField(
            model_name='metrikarequest',
            name='dimensions',
        ),
        migrations.AddField(
            model_name='metrikarequest',
            name='dimension',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ochka.metrikadimension', verbose_name='Параметр группировки, например, ym:s:<attribution>SourceEngine'),
        ),
    ]
