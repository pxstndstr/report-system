# Generated by Django 3.1.6 on 2021-02-02 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0002_auto_20210201_2327'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='type',
            field=models.CharField(choices=[('type_metrika', 'Яндекс.Метрика'), ('type_support', 'Flexites Support'), ('type_elama', 'eLama')], default='type_metrika', max_length=60, verbose_name='Тип настройки'),
        ),
        migrations.AlterField(
            model_name='setting',
            name='first_date',
            field=models.CharField(choices=[('this_month_start', 'Начало месяца отчета'), ('this_month_end', 'Конец месяца отчета'), ('past_month_start', 'Начало прошлого месяца'), ('past_month_end', 'Конец прошлого месяца'), ('last_year_this_month_start', 'Прошлый год - Начало этого месяца'), ('last_year_this_month_end', 'Прошлый год - Конец этого месяца'), ('this_year_start', 'Начало этого года'), ('last_year_start', 'Начало прошлого года'), ('last_year_end', 'Конец прошлого года')], default='this_month_start', max_length=60, verbose_name='Используемая дата начала'),
        ),
        migrations.AlterField(
            model_name='setting',
            name='second_date',
            field=models.CharField(choices=[('this_month_start', 'Начало месяца отчета'), ('this_month_end', 'Конец месяца отчета'), ('past_month_start', 'Начало прошлого месяца'), ('past_month_end', 'Конец прошлого месяца'), ('last_year_this_month_start', 'Прошлый год - Начало этого месяца'), ('last_year_this_month_end', 'Прошлый год - Конец этого месяца'), ('this_year_start', 'Начало этого года'), ('last_year_start', 'Начало прошлого года'), ('last_year_end', 'Конец прошлого года')], default='this_month_end', max_length=60, verbose_name='Используемая дата окончания'),
        ),
    ]
