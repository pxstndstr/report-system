# Generated by Django 3.1 on 2021-03-20 09:38

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0015_auto_20210320_1348'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='report',
            name='html',
        ),
        migrations.AddField(
            model_name='setting',
            name='html',
            field=tinymce.models.HTMLField(null=True),
        ),
    ]
