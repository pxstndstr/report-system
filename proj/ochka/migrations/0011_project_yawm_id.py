# Generated by Django 3.1.6 on 2021-02-03 09:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0010_setting_priority'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='yawm_id',
            field=models.CharField(default='http:vahtovka.com:80', max_length=50, verbose_name='ID в Яндекс.Вебмастер'),
        ),
    ]
