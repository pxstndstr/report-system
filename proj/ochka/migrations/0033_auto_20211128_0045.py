# Generated by Django 3.1 on 2021-11-27 19:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ochka', '0032_auto_20211128_0044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='metrikadimension',
            name='param',
            field=models.CharField(blank=True, max_length=400, null=True, verbose_name='Код параметра группировки'),
        ),
        migrations.AlterField(
            model_name='metrikarequest',
            name='sort_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sort_param', to='ochka.metrikaparam', verbose_name='По какому параметру сортируем?'),
        ),
    ]
