from django.db import models
from tinymce.models import HTMLField


DATE_TYPE = (
    ("this_month_start", "Начало месяца отчета"),
    ("this_month_end", "Конец месяца отчета"),
    ("past_month_start", "Начало прошлого месяца"),
    ("past_month_end", "Конец прошлого месяца"),
    ("last_year_this_month_start", "Прошлый год - Начало этого месяца"),
    ("last_year_this_month_end", "Прошлый год - Конец этого месяца"),
    ("this_year_start", "Начало этого года"),
    ("last_year_start", "Начало прошлого года"),
    ("last_year_end", "Конец прошлого года"),
)

METRIKA_VOC = {
    "Ad traffic": "Переходы по рекламе",
    "Search engine traffic": "Переходы из поисковых систем",
    "Direct traffic": "Прямые заходы",
    "Social network traffic": "Переходы из социальных сетей",
    "Link traffic": "Переходы по ссылкам на сайтах",
    "Internal traffic": "Внутренние переходы",
    "Messenger traffic": "Переходы из мессенджеров",
    "Recommendation system traffic": "Переходы из рекомендательных систем",
    "Cached page traffic": "Переходы с сохраненных страниц",
}

SETTING_TYPE = (
    ("type_metrika", "Яндекс.Метрика"),
    ("type_support", "Flexites Support"),
    ("type_elama", "eLama"),
    ("type_text", "Текст"),
)

MONTH_LIST = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]


class Project(models.Model):
    slug = models.SlugField(max_length=50, unique=True, default="slug", verbose_name="URL проекта в Отчёточке")
    name = models.CharField("Название проекта", max_length=200)
    site = models.CharField("Адрес сайта", max_length=200)
    support_client_id = models.CharField("ID клиента в Support", default="1", max_length=20)
    support_id = models.CharField("ID проекта в Support", default="1", max_length=20)
    metrika_id = models.CharField("ID счетчика Метрики", max_length=20)
    yawm_id = models.CharField("ID в Яндекс.Вебмастер", max_length=50, default="http:vahtovka.com:80")

    report_header = HTMLField(blank=True, null=True)
    report_footer = HTMLField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

class MetrikaParam(models.Model):
    name=models.CharField('Имя параметра Метрики', max_length=400)
    param=models.CharField('Код параметра', max_length=400)
    priority = models.IntegerField("Приоритет в списке настроек", default=500)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Параметр запроса Метрики'
        verbose_name_plural = 'Параметры запроса Метрики'
        ordering = ('priority',)

class MetrikaDimension(models.Model):
    name=models.CharField('Имя параметра группировки', max_length=400)
    param=models.CharField('Код параметра группировки', blank=True, null=True, max_length=400)
    priority = models.IntegerField("Приоритет в списке настроек", default=500)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Параметр группировки отчета Метрики'
        verbose_name_plural = 'Параметры группировки отчета Метрики'
        ordering = ('priority',)

class MetrikaRequest(models.Model):

    name = models.CharField('Суть запроса', max_length=400)
    metrics = models.ManyToManyField(MetrikaParam, related_name="order_metrics")
    dimension = models.ManyToManyField(MetrikaDimension, verbose_name="Параметр группировки")
    sort_by = models.ForeignKey(MetrikaParam, verbose_name="По какому параметру сортируем?", related_name="sort_param", null=True, on_delete=models.CASCADE)
    limit = models.IntegerField('Число записей в ответе', default=10)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Запрос к API Метрики'
        verbose_name_plural = 'Запросы к API Метрики'


class Setting(models.Model):
    name = models.CharField('Название настройки', max_length=200)
    initial_url = models.TextField(default="https://flexites.org/", verbose_name="Изначальный URL настройки")
    commentary = models.TextField(default="Ну бери и пиши блять", verbose_name="Какие данные берем?")
    type = models.CharField(
        "Тип настройки", max_length=60, choices=SETTING_TYPE, default="type_metrika"
    )
    first_date = models.CharField(
        "Используемая дата начала", max_length=60, choices=DATE_TYPE, default="this_month_start"
    )
    second_date = models.CharField(
        "Используемая дата окончания", max_length=60, choices=DATE_TYPE, default="this_month_end"
    )
    global_setting = models.BooleanField("Настройка для всех проектов?", default=True)
    required_setting = models.BooleanField("Обязательная настройка?", default=True)
    metrika_api = models.BooleanField("Обращаемся к API Метрики?", default=False)
    metrika_request = models.ForeignKey(MetrikaRequest, verbose_name="Параметры запроса к API Метрики", null=True, on_delete=models.CASCADE)
    projects = models.ManyToManyField(
        Project,
    )
    priority = models.IntegerField("Приоритет в списке настроек", default=500)

    html = HTMLField("HTML для вставки в отчет", blank=True, null=True)
    html_end = HTMLField("Вторая часть HTML для вставки в отчет", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Настройка'
        verbose_name_plural = 'Настройки'
        ordering = ('priority',)


class Report(models.Model):
    project = models.ForeignKey(Project, verbose_name="Проект", on_delete=models.CASCADE)
    period = models.DateField(verbose_name="Месяц отчета (первый день)", max_length=200)
    url = models.TextField(default="https://flexites.org/", verbose_name="URL отчета в Dropbox Paper")

    def __str__(self):
        return str(self.project)+' | '+str(self.period)

    class Meta:
        verbose_name = 'Отчёт'
        verbose_name_plural = 'Отчёты'
        ordering = ('project', '-period',)
