from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Project, Setting, Report, MONTH_LIST, METRIKA_VOC
from django.http import Http404
from django.views.generic import TemplateView, ListView, DetailView, View
import urllib.parse as urllib
import datetime
import pyperclip
from django.db.models import Q
from tapi_yandex_metrika import YandexMetrikaStats

ACCESS_TOKEN = "AQAAAAAD1V_OAAcEXKZI_-Tdp07hhFzrh4Kxhes"

class HomePageView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context['projects'] = Project.objects.all()
        return context




class ProjectSettings(DetailView):
    model = Project
    model2 = Setting
    context_object_name = 'settings'
    template_name = 'setting_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        proj = get_object_or_404(self.model, slug=self.kwargs['slug'])
        settings = self.model2.objects.filter(Q(projects__slug=self.kwargs['slug']) | Q(global_setting=True))
        if proj.report_header:
            report_body = proj.report_header
        else:
            report_body = ""
        # подключаем клиент API Метрики
        client = YandexMetrikaStats(access_token=ACCESS_TOKEN)
        # проходим по настройкам
        for setting in settings:
            # если настройка содержит только уведомление, то ничего с ней не делаем, просто выводим. иначе:
            if setting.type != 'type_notification':
                # обработка URL настройки
                url = setting.initial_url
                base_url = setting.initial_url.split('?')[0]
                data = urllib.urlparse(url)
                query_data = urllib.parse_qs(data.query)
                date1 = setting.first_date
                date2 = setting.second_date
                period = self.period_processing(date1, date2, setting.type)
                if setting.html:
                    report_body = report_body + str(setting.html)
                # в зависимости от типа настройки меняем разные параметры (можно вынести в отдельную функцию)
                if setting.type == "type_metrika":
                    query_data['id'][0] = proj.metrika_id
                    query_data['period'][0] = period[0]
                    text_metrics=""
                    text_dimensions=""
                    if bool(setting.metrika_request):
                        for metric in setting.metrika_request.metrics.all():
                            text_metrics = text_metrics + metric.param + ', '
                        text_metrics=text_metrics[:-2]
                        for dimension in setting.metrika_request.dimension.all():
                            text_dimensions = text_dimensions + dimension.param + ', '
                        text_dimensions=text_dimensions[:-2]
                        sort="-"+str(setting.metrika_request.sort_by.param)
                    # если эта настройка требует получения данных из Метрики - начинаем обработку данных.
                    # ДОВЕСТИ НУЖНО ДО УМА В МАЕ
                    if (setting.metrika_api & bool(setting.metrika_request)):
                        if setting.metrika_request.dimension is None:
                            params = dict(
                                ids=proj.metrika_id,
                                date1=period[1],
                                date2=period[2],
                                metrics=text_metrics,
                                sort="-"+str(setting.metrika_request.sort_by.param),
                                limit=setting.metrika_request.limit
                                # Other params -> https://yandex.ru/dev/metrika/doc/api2/api_v1/data.html
                            )
                        else:
                            params = dict(
                                ids=proj.metrika_id,
                                date1=period[1],
                                date2=period[2],
                                metrics=text_metrics,
                                dimensions=text_dimensions,
                                sort="-"+str(setting.metrika_request.sort_by.param),
                                limit=setting.metrika_request.limit
                                # Other params -> https://yandex.ru/dev/metrika/doc/api2/api_v1/data.html
                            )
                        report = client.stats().get(params=params)
                        report_body_append = ''
                        report_body_append = report_body_append + '<h2>' + setting.metrika_request.name + '</h2>'
                        report_body_append = report_body_append + '<h3>' + str(period[1]) + ' - ' + str(period[2]) + '</h3>'
                        report_body_append = report_body_append + '<table class="resp-tab"><thead>'
                        for dimension in setting.metrika_request.dimension.all():
                            report_body_append = report_body_append + '<td><strong>' + dimension.name + '<strong></td>'
                        for metric in setting.metrika_request.metrics.all():
                            report_body_append = report_body_append + '<td><strong>' + metric.name + '</strong></td>'
                        report_body_append = report_body_append + '</thead>'
                        for row in report().to_values():
                            report_body_append = report_body_append + '<tr>'
                            for item in row:
                                try:
                                    float(item)
                                except ValueError:
                                    report_body_append = report_body_append + '<td>' + str(item) + '</td>'
                                else:
                                    if item.is_integer():
                                        report_body_append = report_body_append + '<td>' + str(int(item)) + '</td>'
                                    else:
                                        report_body_append = report_body_append + '<td>' + str(round(item, 2)) + '</td>'
                            report_body_append = report_body_append + '</tr>'
                        report_body = report_body + report_body_append + '</table>'
                if setting.type == "type_support":
                    query_data['work_date__ge'][0] = period[0]
                    query_data['work_date__le'][0] = period[1]
                    query_data['client_id'][0] = proj.support_client_id
                    query_data['project_id'][0] = proj.support_id
                setting.initial_url = base_url + '?' + urllib.urlencode(query_data, doseq=True)
                # конец обработки URL
                if setting.html_end:
                    report_body = report_body + str(setting.html_end)
        if proj.report_footer:
            report_body = report_body + proj.report_footer
        for key, val in METRIKA_VOC.items():
            report_body = report_body.replace(key, val)
        report_body = self.body_processing(report_body) # подставляем актуальные даты и другие значения
        # pyperclip.copy(report_body) # копируем тело отчета в буфер обмена
        context['settings'] = settings
        context['this_project'] = proj
        context['projects'] = self.model.objects.all()
        context['reports'] = Report.objects.all().filter(project=proj)
        context['report_body'] = report_body
        return context

    def period_processing(self, date1, date2, type):


        now = datetime.datetime.now()

        # записываем сегодняшнюю дату в числах
        year1 = int(now.year)
        month1 = int(now.month)
        day1 = 1  # отчеты всегда строим с первого дня месяца

        # по умолчанию предполагаем, что месяц и год второй даты совпадают с первой
        year2 = int(now.year)
        month2 = int(now.month)
        day2 = int(31)  # чаще всего в месяце 31 день, проверять это будем позже

        # вычисляем первую дату
        if date1 == 'this_month_start':
            month1 = month1 - 1

        if date1 == 'last_year_this_month_start':
            month1 = month1 - 1
            year1 = year1 - 1

        if date1 == 'past_month_start':
            month1 = month1 - 2

        if date1 == 'this_year_start':
            month1 = 1

        if date1 == 'last_year_start':
            month1 = 1
            year1 = year1 - 1

        # вычисляем вторую дату
        if date2 == 'this_month_end':
            month2 = month2 - 1

        if date2 == 'past_month_end':
            month2 = month2 - 2

        if date2 == 'last_year_this_month_end':
            year2 = year2 - 1
            month2 = month2 - 1

        if date2 == 'last_year_end':
            year2 = year2 - 1
            month2 = 12

        # пересчет дней исходя из месяца, перевод месяцев в положительные числа
        # первая дата

        if month1 == 0:  # декабрь прошлого года, 31 день
            month1 = 12
            year1 = year1 - 1

        if month1 == -1:  # ноябрь пролого года, 30 дней
            month1 = 11
            year1 = year1 - 1

        # вторая дата
        if month2 == 2:  # февраль, проверяем на високосный год
            if year2 % 4 == 0:
                day2 = 29
            else:
                day2 = 28

        if month2 == 0:  # декабрь прошлого года, 31 день
            month2 = 12
            year2 = year2 - 1

        if month2 == -1:  # ноябрь пролого года, 30 дней
            month2 = 11
            year2 = year2 - 1
            day2 = 30

        if month2 == 4 or month2 == 6 or month2 == 9 or month2 == 11:  # оставшиеся месяцы по 30 дней
            day2 = 30

        # преобразования в строку (добавить нули туда, где надо)
        if day1 < 10:
            day1 = '0' + str(day1)
        else:
            day1 = str(day1)

        if month1 < 10:
            month1 = '0' + str(month1)
        else:
            month1 = str(month1)

        year1 = str(year1)

        if day2 < 10:
            day2 = '0' + str(day2)
        else:
            day2 = str(day2)

        if month2 < 10:
            month2 = '0' + str(month2)
        else:
            month2 = str(month2)

        year2 = str(year2)

        period = ['start-end', 'start', 'end']

        # получение конечной строки для Метрики
        if type == "type_metrika":
            period[0] = year1 + '-' + month1 + '-' + day1 + ':' + year2 + '-' + month2 + '-' + day2
            period[1] = year1 + '-' + month1 + '-' + day1
            period[2] = year2 + '-' + month2 + '-' + day2

        if type == "type_support":
            period[0] = day1 + '.' + month1 + '.' + year1
            period[1] = day2 + '.' + month2 + '.' + year2

        # получение конечной строки для Support
        return period

    def body_processing(self, report_body):

        now = datetime.datetime.now()
        month = int(now.month)
        year = int(now.year)
        if month == 1:
            month = 12
            year = year-1
        else:
            month = month-1
        nice_date = MONTH_LIST[month-1]+" "+str(year)
        report_body = report_body.replace('###var_this_month###', nice_date)

        # делаем то же самое для предыдущего месяца
        if month == 1:
            month = 12
            year = year-1
        else:
            month = month-1
        nice_date = MONTH_LIST[month-1]+" "+str(year)
        report_body = report_body.replace('###var_last_month###', nice_date)
        return report_body
