from django import template

from ochka.models import (
    Project,
)

register = template.Library()


@register.inclusion_tag('sidebar.html', takes_context=True)
def get_projects(context):
    projects = Project.objects.all()
    return {'my_projects': projects}
