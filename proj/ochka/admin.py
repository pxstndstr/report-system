from django.contrib import admin
from .models import Project, Setting, Report, MetrikaRequest, MetrikaParam, MetrikaDimension


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ("name", "priority")


@admin.register(Project)
class SettingAdmin(admin.ModelAdmin):
    list_display = ("name", "site")


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ("project", "period")

@admin.register(MetrikaRequest)
class ReportAdmin(admin.ModelAdmin):
    list_display = ("name", )

@admin.register(MetrikaParam)
class ReportAdmin(admin.ModelAdmin):
    list_display = ("name", "param")

@admin.register(MetrikaDimension)
class ReportAdmin(admin.ModelAdmin):
    list_display = ("name", )
